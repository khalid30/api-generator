module github.com/khalid301/api-generator/generator

go 1.16

require github.com/kelseyhightower/envconfig v1.4.0

require (
	github.com/iancoleman/strcase v0.2.0
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.4 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
)
