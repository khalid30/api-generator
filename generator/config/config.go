package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	HTTPPort string `envconfig:"HTTP_PORT" default:"8080"`
	DBType   string `envconfig:"DB_TYPE" default:"mysql"`

	Host     string `envconfig:"GENERATOR__HOST" default:"localhost"`
	Port     string `envconfig:"GENERATOR__PORT" default:"3306"`
	Username string `envconfig:"GENERATOR__USERNAME" default:"root"`
	Password string `envconfig:"GENERATOR__PASSWORD" default:""`
	DBName   string `envconfig:"GENERATOR__DB_NAME" default:""`

	RedisHost     string `envconfig:"GENERATOR__REDIS_HOST" default:"127.0.0.1"`
	RedisPort     string `envconfig:"GENERATOR__REDIS_PORT" default:"6379"`
	RedisPassword string `envconfig:"GENERATOR__REDIS_PASSWORD" default:""`
	RedisMaxIdle  int    `envconfig:"GENERATOR__REDIS_MAX_IDLE" default:"100"`

	SSLMode         bool   `envconfig:"GENERATOR__SSL_MODE" default:"true"`
	MaxIdleConns    int    `envconfig:"GENERATOR__MAX_IDLE_CONNECTION" default:"5"`
	MaxOpenConns    int    `envconfig:"GENERATOR__MAX_OPEN_CONNECTION" default:"10"`
	ConnMaxLifetime int    `envconfig:"GENERATOR__MAX_LIFETIME_CONNECTION" default:"10"`
	LogMode         bool   `envconfig:"GENERATOR__LOG_MODE" default:"true"`
	SingularTable   bool   `envconfig:"GENERATOR__SINGULAR_TABLE" default:"true"`
	ParseTime       bool   `envconfig:"GENERATOR__PARSE_TIME" default:"true"`
	Charset         string `envconfig:"GENERATOR__CHARSET" default:"utf8mb4"`
	Loc             string `envconfig:"GENERATOR__LOC" default:"Local"`

	ResultDir        string   `envconfig:"GENERATOR__RESULT_DIR" default:"/build"`
	PackagePrefix    string   `envconfig:"GENERATOR__PACKAGE_PREFIX" default:""`
	IncludeAllTables bool     `envconfig:"GENERATOR__INCLUDE_ALL_TABLES" default:"true"`
	IncludedTables   []string `envconfig:"GENERATOR__INCLUDED_TABLES" default:"[]"`
	ExcludedTables   []string `envconfig:"GENERATOR__EXCLUDED_TABLES" default:"[]"`
}

func Get() Config {
	cfg := Config{}
	envconfig.MustProcess("", &cfg)
	return cfg
}
