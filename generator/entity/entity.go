package entity

type TableName struct {
	TableName string `gorm:"column:TABLE_NAME"`
}

type ColumnNames struct {
	Field string `gorm:"column:Field"`
	Type  string `gorm:"column:Type"`
	Null  string `gorm:"column:Null"`
	Key   string `gorm:"column:Key"`
}

var MappingDBTypeToStructType = map[string]string{
	"int":      "int32",
	"varchar":  "string",
	"char":     "string",
	"text":     "string",
	"datetime": "time.Time",
}
