package module

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/khalid301/api-generator/generator/config"
)

func DirGenerator(cfg config.Config) error {
	if cfg.ResultDir != "." {
		if _, err := os.Stat(fmt.Sprintf(`..%s`, cfg.ResultDir)); os.IsNotExist(err) {
			// create new generator result directory
			log.Printf("Creating '%v' diretory as generator result root dir", cfg.ResultDir)
			err = os.Mkdir(fmt.Sprintf("..%s", cfg.ResultDir), 0755)
			if err != nil {
				return err
			}
			log.Printf("successfully created '%v' diretory", cfg.ResultDir)
		}
	}

	essentialDir := []string{
		"core",
		"core/entity",
		"core/entity/gen_entity",
		"core/module",
		"core/module/gen_module",
		"core/repository",
		"core/repository/gen_repository",
		"repository",
		"repository/gen_repository",
		"handler",
		"handler/api",
		"config",
		"pkg",
		"pkg/helper",
		"pkg/conn",
	}

	for i := 0; i < len(essentialDir); i++ {
		// creating directory if not exist
		dirString := fmt.Sprintf(`..%s/%s`, cfg.ResultDir, essentialDir[i])
		if _, err := os.Stat(dirString); os.IsNotExist(err) {
			// create directory
			err = os.Mkdir(dirString, 0755)
			if err != nil {
				return err
			}
			log.Printf("successfully created '%v' diretory", dirString)
		}
	}

	files, err := ioutil.ReadDir("templates/helper")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		// get content of this file
		b, err := ioutil.ReadFile(fmt.Sprintf("templates/helper/%s", file.Name()))
		if err != nil {
			panic(err)
		}
		str := string(b)

		// create new file using file content above
		fileName := fmt.Sprintf("..%s/pkg/helper/%s", cfg.ResultDir, strings.ReplaceAll(file.Name(), "txt", "go"))
		fileContent := []byte(str)
		err = os.WriteFile(fileName, fileContent, 0644)
		if err != nil {
			panic(err)
		}
	}

	// setting up main.go
	bMain, err := ioutil.ReadFile("templates/main_template.txt")
	if err != nil {
		panic(err)
	}
	strMain := string(bMain)

	packagePrefix := fmt.Sprintf("%s%s", cfg.PackagePrefix, cfg.ResultDir)
	strMain = strings.ReplaceAll(strMain, "[packagePrefix]", packagePrefix)

	// create main.go only when file does not exist
	mainFileName := fmt.Sprintf("..%s/main.go", cfg.ResultDir)
	if _, err := os.Stat(mainFileName); errors.Is(err, os.ErrNotExist) {
		err = os.WriteFile(mainFileName, []byte(strMain), 0644)
		if err != nil {
			panic(err)
		}
	}

	return nil
}
