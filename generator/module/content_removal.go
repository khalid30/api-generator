package module

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/khalid301/api-generator/generator/config"
)

func ContentDirRemoval(cfg config.Config) error {
	contentRemovalDir := []string{
		"core/entity/gen_entity",
		"core/module/gen_module",
		"core/repository/gen_repository",
		"repository/gen_repository",
	}

	for i := 0; i < len(contentRemovalDir); i++ {
		dir := fmt.Sprintf("..%s/%s", cfg.ResultDir, contentRemovalDir[i])
		err := RemoveAllContents(dir)
		if err != nil {
			panic(err)
		}
		log.Printf("successfully deleting previcously generated files in '%v' diretory", contentRemovalDir[i])
	}

	return nil
}

func RemoveAllContents(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	return nil
}
