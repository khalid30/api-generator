package module

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/khalid301/api-generator/generator/config"
)

func ConnGenerator(cfg config.Config) error {
	entityDir := fmt.Sprintf(`..%s/pkg/conn`, cfg.ResultDir)
	packagePrefix := fmt.Sprintf("%s%s", cfg.PackagePrefix, cfg.ResultDir)
	log.Printf("Generating conn files in '%s' ...", entityDir)

	// mysql conn generation
	b, err := ioutil.ReadFile("templates/conn/mysql.txt")
	if err != nil {
		panic(err)
	}
	strMysql := string(b)

	strMysql = strings.ReplaceAll(strMysql, "[packagePrefix]", packagePrefix)

	fileName := fmt.Sprintf("%s/mysql.go", entityDir)
	fileContent := []byte(strMysql)
	err = os.WriteFile(fileName, fileContent, 0644)
	if err != nil {
		panic(err)
	}

	// redis conn generation
	br, err := ioutil.ReadFile("templates/conn/redis.txt")
	if err != nil {
		panic(err)
	}
	strRedis := string(br)

	strRedis = strings.ReplaceAll(strRedis, "[packagePrefix]", packagePrefix)

	fileNameRedis := fmt.Sprintf("%s/redis.go", entityDir)
	fileContentRedis := []byte(strRedis)
	err = os.WriteFile(fileNameRedis, fileContentRedis, 0644)
	if err != nil {
		panic(err)
	}

	return nil
}
