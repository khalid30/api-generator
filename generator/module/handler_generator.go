package module

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/iancoleman/strcase"
	"github.com/jinzhu/gorm"
	"github.com/khalid301/api-generator/generator/config"
	"github.com/khalid301/api-generator/generator/entity"
)

func HandlerGenerator(cfg config.Config, db *gorm.DB) error {
	entityDir := fmt.Sprintf(`..%s/handler/api`, cfg.ResultDir)
	packagePrefix := fmt.Sprintf("%s%s", cfg.PackagePrefix, cfg.ResultDir)
	log.Printf("Generating Handler file in '%s' ...", entityDir)

	b, err := ioutil.ReadFile("templates/handler_template.txt")
	if err != nil {
		panic(err)
	}
	str := string(b)

	bEntity, err := ioutil.ReadFile("templates/handler_entity_template.txt")
	if err != nil {
		panic(err)
	}
	strEntity := string(bEntity)

	bEntityDelete, err := ioutil.ReadFile("templates/handler_entity_delete_template.txt")
	if err != nil {
		panic(err)
	}
	strEntityDelete := string(bEntityDelete)

	generatedString := str

	tableNames := []entity.TableName{}
	sql := fmt.Sprintf("SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = '%s'", cfg.DBName)
	if err := db.Raw(sql).Scan(&tableNames).Error; err != nil {
		panic(err)
	}

	usecaseListString := ""
	usecaseInitiatorListString := ""
	interfaceGeneratedStringList := ""
	usecaseListStringVariableOnlyList := ""
	funcHandlerStringList := ""

	// loop every table name
	for i := 0; i < len(tableNames); i++ {
		snakeCaseTableName := tableNames[i].TableName
		pascalCaseTableName := strcase.ToCamel(tableNames[i].TableName)
		pascalCaseTableNameLowercase := strcase.ToLowerCamel(tableNames[i].TableName)

		usecaseList := fmt.Sprintf("	%sUc gen_module.%sUsecase", pascalCaseTableNameLowercase, pascalCaseTableName)
		usecaseListVariableOnly := fmt.Sprintf("		%sUc: %sUc", pascalCaseTableNameLowercase, pascalCaseTableNameLowercase)

		usecaseListString = fmt.Sprintf("%s\n%s", usecaseListString, usecaseList)
		usecaseInitiatorListString = fmt.Sprintf("%s\n%s,", usecaseInitiatorListString, usecaseList)
		usecaseListStringVariableOnlyList = fmt.Sprintf("%s\n%s,", usecaseListStringVariableOnlyList, usecaseListVariableOnly)

		primaryKey := "Id"
		primaryKeyLowercase := "id"
		hasDeletedAt := false
		getParameterString := ""
		getParameterStringNoType := ""
		requestParameterStringList := ""
		requestPrimaryKeyParameterStringList := ""
		requestParameterBodyStringList := ""
		createParameterStringList := ""

		columnNames := []entity.ColumnNames{}
		sqlColumnNames := fmt.Sprintf("SHOW COLUMNS FROM `%s`", snakeCaseTableName)
		if err := db.Raw(sqlColumnNames).Scan(&columnNames).Error; err != nil {
			panic(err)
		}

		for j := 0; j < len(columnNames); j++ {
			pascalCaseColumnName := strcase.ToCamel(columnNames[j].Field)
			pascalCaseColumnNameLowercase := strcase.ToLowerCamel(columnNames[j].Field)

			columnType := "string"
			for key, element := range entity.MappingDBTypeToStructType {
				if strings.Contains(columnNames[j].Type, key) {
					columnType = element
				}
			}

			if columnNames[j].Field == "deleted_at" {
				hasDeletedAt = true
			}

			if columnNames[j].Key == "PRI" {
				primaryKey = pascalCaseColumnName
				primaryKeyLowercase = pascalCaseColumnNameLowercase
			}

			if !strings.Contains(columnNames[j].Field, "created") && !strings.Contains(columnNames[j].Field, "updated") && !strings.Contains(columnNames[j].Field, "deleted") {
				getParameterString = fmt.Sprintf("%s, %s %s", getParameterString, pascalCaseColumnNameLowercase, columnType)
				getParameterStringNoType = fmt.Sprintf("%s, %s", getParameterStringNoType, pascalCaseColumnNameLowercase)

				// collecting request parameter
				requestParameterString := fmt.Sprintf(`	%s := c.Query("%s")`, pascalCaseColumnNameLowercase, columnNames[j].Field)
				requestParameterBodyString := fmt.Sprintf(`	%s := requestBody["%s"].(string)`, pascalCaseColumnNameLowercase, columnNames[j].Field)

				if columnType == "int32" {
					requestParameterString = fmt.Sprintf("	%si64, _ := strconv.ParseInt(c.Query(\"%s\"), 10, 32)\n	%s := int32(%si64)", pascalCaseColumnNameLowercase, columnNames[j].Field, pascalCaseColumnNameLowercase, pascalCaseColumnNameLowercase)
					requestParameterBodyString = fmt.Sprintf("	%s := requestBody[\"%s\"].(int32)", pascalCaseColumnNameLowercase, columnNames[j].Field)
				}
				if columnType == "time.Time" {
					requestParameterString = fmt.Sprintf("	%s, _ := time.Parse(time.RFC3339, c.Query(\"%s\"))", pascalCaseColumnNameLowercase, columnNames[j].Field)
					requestParameterBodyString = fmt.Sprintf("	%s, _ := time.Parse(time.RFC3339, requestBody[\"%s\"].(string))", pascalCaseColumnNameLowercase, columnNames[j].Field)
				}

				requestParameterStringList = fmt.Sprintf("%s\n%s", requestParameterStringList, requestParameterString)
				requestParameterBodyStringList = fmt.Sprintf("%s\n%s", requestParameterBodyStringList, requestParameterBodyString)

				if columnNames[j].Key == "PRI" {
					requestPrimaryKeyParameterStringList = requestParameterString
				} else {
					createParameterStringList = fmt.Sprintf("%s, %s", createParameterStringList, pascalCaseColumnNameLowercase)
				}
			}

		}

		interfaceGeneratedString := `
		Get[entityName](c *gin.Context)
		Get[entityName]By[primaryKey](c *gin.Context)
		Create[entityName](c *gin.Context)
		Update[entityName]By[primaryKey](c *gin.Context)
		[deleteInterfaceSection]`

		funcHandlerString := strEntity

		if hasDeletedAt {
			interfaceGeneratedString = strings.ReplaceAll(interfaceGeneratedString, "[deleteInterfaceSection]", "Delete[entityName]ByID(c *gin.Context)")
			funcHandlerString = strings.ReplaceAll(funcHandlerString, "[deleteSection]", strEntityDelete)
		} else {
			interfaceGeneratedString = strings.ReplaceAll(interfaceGeneratedString, "[deleteInterfaceSection]", "")
			funcHandlerString = strings.ReplaceAll(funcHandlerString, "[deleteSection]", "")
		}

		interfaceGeneratedString = strings.ReplaceAll(interfaceGeneratedString, "[entityName]", pascalCaseTableName)
		interfaceGeneratedString = strings.ReplaceAll(interfaceGeneratedString, "[primaryKey]", primaryKey)

		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[entityName]", pascalCaseTableName)
		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[entityNameLowercase]", pascalCaseTableNameLowercase)
		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[primaryKey]", primaryKey)
		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[primaryKeyLowercase]", primaryKeyLowercase)
		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[getParameterSection]", fmt.Sprintf(", %s", getParameterString[2:]))
		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[getParameterSectionNoType]", fmt.Sprintf(", %s", getParameterStringNoType[2:]))
		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[requestParameterSection]", requestParameterStringList)
		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[requestParameterBodySection]", requestParameterBodyStringList)
		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[requestPrimaryKeyParameterSection]", requestPrimaryKeyParameterStringList)
		funcHandlerString = strings.ReplaceAll(funcHandlerString, "[createParameterSection]", createParameterStringList)

		interfaceGeneratedStringList = fmt.Sprintf("%s\n%s", interfaceGeneratedStringList, interfaceGeneratedString[1:])
		funcHandlerStringList = fmt.Sprintf("%s\n%s", funcHandlerStringList, funcHandlerString)
	}

	generatedString = strings.ReplaceAll(generatedString, "[packagePrefix]", packagePrefix)
	generatedString = strings.ReplaceAll(generatedString, "[usecaseListSection]", usecaseListString[2:])
	generatedString = strings.ReplaceAll(generatedString, "[usecaseInitiatorListSection]", usecaseInitiatorListString[2:])
	generatedString = strings.ReplaceAll(generatedString, "[usecaseListVariableOnlySection]", usecaseListStringVariableOnlyList)
	generatedString = strings.ReplaceAll(generatedString, "[interfaceSection]", interfaceGeneratedStringList[1:])
	generatedString = strings.ReplaceAll(generatedString, "[funcHandlerSection]", funcHandlerStringList)

	fileName := fmt.Sprintf("%s/gen_handler.go", entityDir)
	fileContent := []byte(generatedString)
	err = os.WriteFile(fileName, fileContent, 0644)
	if err != nil {
		panic(err)
	}

	return nil
}
