package module

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/iancoleman/strcase"
	"github.com/jinzhu/gorm"
	"github.com/khalid301/api-generator/generator/config"
	"github.com/khalid301/api-generator/generator/entity"
)

func CoreRepositoryGenerator(cfg config.Config, db *gorm.DB) error {
	entityDir := fmt.Sprintf(`..%s/core/repository/gen_repository`, cfg.ResultDir)
	packagePrefix := fmt.Sprintf("%s%s", cfg.PackagePrefix, cfg.ResultDir)
	log.Printf("Generating Core Repository files in '%s' ...", entityDir)

	tableNames := []entity.TableName{}
	sql := fmt.Sprintf("SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = '%s'", cfg.DBName)
	if err := db.Raw(sql).Scan(&tableNames).Error; err != nil {
		panic(err)
	}

	// loop every table name
	for i := 0; i < len(tableNames); i++ {
		log.Printf("Processing %s ...", tableNames[i].TableName)

		snakeCaseTableName := tableNames[i].TableName
		pascalCaseTableName := strcase.ToCamel(tableNames[i].TableName)

		b, err := ioutil.ReadFile("templates/core_repository_template.txt")
		if err != nil {
			panic(err)
		}
		str := string(b)

		generatedString := str

		hasDeletedAt := false
		getParameterString := ""
		createParamesterString := ""
		createParamesterStringNoType := ""
		primaryKey := "Id"
		primaryKeyType := "int32"

		columnNames := []entity.ColumnNames{}
		sqlColumnNames := fmt.Sprintf("SHOW COLUMNS FROM `%s`", snakeCaseTableName)
		if err := db.Raw(sqlColumnNames).Scan(&columnNames).Error; err != nil {
			panic(err)
		}

		for j := 0; j < len(columnNames); j++ {
			if columnNames[j].Field == "deleted_at" {
				hasDeletedAt = true
			}

			pascalCaseColumnName := strcase.ToCamel(columnNames[j].Field)
			pascalCaseColumnNameLowercase := strcase.ToLowerCamel(columnNames[j].Field)

			columnType := "string"
			for key, element := range entity.MappingDBTypeToStructType {
				if strings.Contains(columnNames[j].Type, key) {
					columnType = element
				}
			}

			if columnNames[j].Field != "id" {
				createParamesterString = fmt.Sprintf("%s, %s %s", createParamesterString, pascalCaseColumnNameLowercase, columnType)
				createParamesterStringNoType = fmt.Sprintf("%s, %s", createParamesterStringNoType, pascalCaseColumnNameLowercase)
			}

			if !strings.Contains(columnNames[j].Field, "created") && !strings.Contains(columnNames[j].Field, "updated") && !strings.Contains(columnNames[j].Field, "deleted") {
				getParameterString = fmt.Sprintf("%s, %s %s", getParameterString, pascalCaseColumnNameLowercase, columnType)
			}

			if columnNames[j].Key == "PRI" {
				primaryKey = pascalCaseColumnName
				primaryKeyType = columnType
			}
		}

		if hasDeletedAt {
			generatedString = strings.ReplaceAll(generatedString, "[deleteSection]", "Delete[entityName]ByID(ID int32) (ok bool, err error)")
		} else {
			generatedString = strings.ReplaceAll(generatedString, "[deleteSection]", "")
		}

		generatedString = strings.ReplaceAll(generatedString, "[packagePrefix]", packagePrefix)
		generatedString = strings.ReplaceAll(generatedString, "[entityName]", pascalCaseTableName)
		generatedString = strings.ReplaceAll(generatedString, "[createParameterSection]", createParamesterString[2:])
		generatedString = strings.ReplaceAll(generatedString, "[createParameterSectionNoType]", createParamesterStringNoType[2:])
		generatedString = strings.ReplaceAll(generatedString, "[getParameterSection]", fmt.Sprintf(", %s", getParameterString[2:]))
		generatedString = strings.ReplaceAll(generatedString, "[primaryKey]", primaryKey)
		generatedString = strings.ReplaceAll(generatedString, "[primaryKeyType]", primaryKeyType)

		if strings.Contains(createParamesterString[2:], "time.Time") {
			generatedString = strings.ReplaceAll(generatedString, "[timeSection]", `"time"`)
		} else {
			generatedString = strings.ReplaceAll(generatedString, "[timeSection]", ``)
		}

		fileName := fmt.Sprintf("%s/%s_repository.go", entityDir, snakeCaseTableName)
		fileContent := []byte(generatedString)
		err = os.WriteFile(fileName, fileContent, 0644)
		if err != nil {
			panic(err)
		}
	}

	return nil
}
