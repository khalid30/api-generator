package module

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/iancoleman/strcase"
	"github.com/jinzhu/gorm"
	"github.com/khalid301/api-generator/generator/config"
	"github.com/khalid301/api-generator/generator/entity"
)

func RouterGenerator(cfg config.Config, db *gorm.DB) error {
	entityDir := fmt.Sprintf(`..%s/handler/api`, cfg.ResultDir)
	packagePrefix := fmt.Sprintf("%s%s", cfg.PackagePrefix, cfg.ResultDir)
	log.Printf("Generating Router file in '%s' ...", entityDir)

	b, err := ioutil.ReadFile("templates/router_template.txt")
	if err != nil {
		panic(err)
	}
	str := string(b)
	generatedString := str

	repositoryPackageVariableStringList := ""
	repositoryVariableSectionList := ""
	usecaseVariableSectionList := ""
	handlerVariableSectionList := ""
	routerSectionList := ""

	tableNames := []entity.TableName{}
	sql := fmt.Sprintf("SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = '%s'", cfg.DBName)
	if err := db.Raw(sql).Scan(&tableNames).Error; err != nil {
		panic(err)
	}

	// loop every table name
	for i := 0; i < len(tableNames); i++ {
		log.Printf("Processing %s for router...", tableNames[i].TableName)

		snakeCaseTableName := tableNames[i].TableName
		pascalCaseTableName := strcase.ToCamel(tableNames[i].TableName)
		pascalCaseTableNameLowercase := strcase.ToLowerCamel(tableNames[i].TableName)
		primaryKey := "Id"
		primaryKeySnakecase := "id"

		columnNames := []entity.ColumnNames{}
		sqlColumnNames := fmt.Sprintf("SHOW COLUMNS FROM `%s`", snakeCaseTableName)
		if err := db.Raw(sqlColumnNames).Scan(&columnNames).Error; err != nil {
			panic(err)
		}

		for j := 0; j < len(columnNames); j++ {
			pascalCaseColumnName := strcase.ToCamel(columnNames[j].Field)
			if columnNames[j].Key == "PRI" {
				primaryKey = pascalCaseColumnName
				primaryKeySnakecase = columnNames[j].Field
			}
		}

		repositoryPackageVariableString := fmt.Sprintf(`	"[packagePrefix]/repository/gen_repository/%s_repository"`, snakeCaseTableName)
		repositoryPackageVariableStringList = fmt.Sprintf("%s\n%s", repositoryPackageVariableStringList, repositoryPackageVariableString)

		repositoryVariableSection := fmt.Sprintf(`		%sRepository = %s_repository.New(db)`, pascalCaseTableName, snakeCaseTableName)
		repositoryVariableSectionList = fmt.Sprintf("%s\n%s", repositoryVariableSectionList, repositoryVariableSection)

		usecaseVariableSection := fmt.Sprintf(`		%sUsecase = gen_module.New%sUsecase(%sRepository)`, pascalCaseTableNameLowercase, pascalCaseTableName, pascalCaseTableName)
		usecaseVariableSectionList = fmt.Sprintf("%s\n%s", usecaseVariableSectionList, usecaseVariableSection)

		handlerVariableSection := fmt.Sprintf(`			%sUsecase,`, pascalCaseTableNameLowercase)
		handlerVariableSectionList = fmt.Sprintf("%s\n%s", handlerVariableSectionList, handlerVariableSection)

		routerSection := fmt.Sprintf(`
	router.GET("/%s", handler.Get%s)
	router.POST("/%s", handler.Create%s)
	router.PUT("/%s/:%s", handler.Get%sBy%s)
		`, snakeCaseTableName, pascalCaseTableName, snakeCaseTableName, pascalCaseTableName, snakeCaseTableName, primaryKeySnakecase, pascalCaseTableName, primaryKey)
		routerSectionList = fmt.Sprintf("%s\n%s", routerSectionList, routerSection)
	}

	generatedString = strings.ReplaceAll(generatedString, "[repositoryPackageSection]", repositoryPackageVariableStringList[2:])
	generatedString = strings.ReplaceAll(generatedString, "[repositoryVariableSection]", repositoryVariableSectionList[3:])
	generatedString = strings.ReplaceAll(generatedString, "[usecaseVariableSection]", usecaseVariableSectionList[3:])
	generatedString = strings.ReplaceAll(generatedString, "[handlerVariableSection]", handlerVariableSectionList[4:])
	generatedString = strings.ReplaceAll(generatedString, "[routerSection]", routerSectionList)
	generatedString = strings.ReplaceAll(generatedString, "[packagePrefix]", packagePrefix)

	fileName := fmt.Sprintf("%s/gen_router.go", entityDir)
	fileContent := []byte(generatedString)
	err = os.WriteFile(fileName, fileContent, 0644)
	if err != nil {
		panic(err)
	}

	return nil
}
