package module

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/iancoleman/strcase"
	"github.com/jinzhu/gorm"
	"github.com/khalid301/api-generator/generator/config"
	"github.com/khalid301/api-generator/generator/entity"
)

func RepositoryGenerator(cfg config.Config, db *gorm.DB) error {
	entityDir := fmt.Sprintf(`..%s/repository/gen_repository`, cfg.ResultDir)
	packagePrefix := fmt.Sprintf("%s%s", cfg.PackagePrefix, cfg.ResultDir)
	log.Printf("Generating Repository files in '%s' ...", entityDir)

	tableNames := []entity.TableName{}
	sql := fmt.Sprintf("SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = '%s'", cfg.DBName)
	if err := db.Raw(sql).Scan(&tableNames).Error; err != nil {
		panic(err)
	}

	// loop every table name
	for i := 0; i < len(tableNames); i++ {
		log.Printf("Processing %s ...", tableNames[i].TableName)

		snakeCaseTableName := tableNames[i].TableName
		pascalCaseTableName := strcase.ToCamel(tableNames[i].TableName)
		tableRepositoryDirName := fmt.Sprintf("%s/%s_repository", entityDir, snakeCaseTableName)

		b, err := ioutil.ReadFile("templates/repository_template.txt")
		if err != nil {
			panic(err)
		}
		str := string(b)

		generatedString := str

		columnNames := []entity.ColumnNames{}
		sqlColumnNames := fmt.Sprintf("SHOW COLUMNS FROM `%s`", snakeCaseTableName)
		if err := db.Raw(sqlColumnNames).Scan(&columnNames).Error; err != nil {
			panic(err)
		}

		columnStringList := ``
		columnStringListInverse := ``

		hasDeletedAt := false
		getParameterString := ""
		getParameterLogicString := ""
		createParamesterString := ""
		createParamesterStringNoType := ""
		recordFieldString := ""
		primaryKey := "Id"
		primaryKeyType := "int32"

		for j := 0; j < len(columnNames); j++ {
			log.Printf("- %s", columnNames[j].Field)

			pascalCaseColumnName := strcase.ToCamel(columnNames[j].Field)
			pascalCaseColumnNameLowercase := strcase.ToLowerCamel(columnNames[j].Field)

			if columnNames[j].Field == "deleted_at" {
				hasDeletedAt = true
			}

			columnType := "string"
			for key, element := range entity.MappingDBTypeToStructType {
				if strings.Contains(columnNames[j].Type, key) {
					columnType = element
				}
			}

			if columnNames[j].Field != "id" {
				createParamesterString = fmt.Sprintf("%s, %s %s", createParamesterString, pascalCaseColumnNameLowercase, columnType)
				createParamesterStringNoType = fmt.Sprintf("%s, %s", createParamesterStringNoType, pascalCaseColumnNameLowercase)
				recordFieldString = fmt.Sprintf("%s\n		%s: %s,", recordFieldString, pascalCaseColumnName, pascalCaseColumnNameLowercase)
			}

			if !strings.Contains(columnNames[j].Field, "created") && !strings.Contains(columnNames[j].Field, "updated") && !strings.Contains(columnNames[j].Field, "deleted") {
				getParameterString = fmt.Sprintf("%s, %s %s", getParameterString, pascalCaseColumnNameLowercase, columnType)

				if columnType == "string" {
					logicOperator := `!= ""`
					logicString := fmt.Sprintf(`	if %s %s {
		keywordString := "%%" + %s + "%%"
		db = db.Where("%s like ?", keywordString)
	}
					`, pascalCaseColumnNameLowercase, logicOperator, pascalCaseColumnNameLowercase, columnNames[j].Field)
					getParameterLogicString = fmt.Sprintf("%s\n%s", getParameterLogicString, logicString)
				}

				if columnType == "int32" {
					logicOperator := `> 0`
					logicString := fmt.Sprintf(`	if %s %s {
		db = db.Where("%s = ?", %s)
	}
					`, pascalCaseColumnNameLowercase, logicOperator, columnNames[j].Field, pascalCaseColumnNameLowercase)
					getParameterLogicString = fmt.Sprintf("%s\n%s", getParameterLogicString, logicString)
				}

				if columnType == "time.Time" {
					logicString := fmt.Sprintf(`	if !%s.IsZero() {
						db = db.Where("%s = ?", %s)
					}
					`, pascalCaseColumnNameLowercase, columnNames[j].Field, pascalCaseColumnNameLowercase)
					getParameterLogicString = fmt.Sprintf("%s\n%s", getParameterLogicString, logicString)
				}
			}

			if columnNames[j].Key == "PRI" {
				primaryKey = pascalCaseColumnName
				primaryKeyType = columnType
			}

			// logic for inserting column related string
			columnString := fmt.Sprintf("		%s: record.%s,", pascalCaseColumnName, pascalCaseColumnName)
			columnStringList = fmt.Sprintf("%s\n%s", columnStringList, columnString)

			columnStringInverse := fmt.Sprintf("		%s: record.%s,", pascalCaseColumnName, pascalCaseColumnName)
			columnStringListInverse = fmt.Sprintf("%s\n%s", columnStringListInverse, columnStringInverse)
		}

		generatedString = strings.ReplaceAll(generatedString, "[packagePrefix]", packagePrefix)
		generatedString = strings.ReplaceAll(generatedString, "[entityName]", pascalCaseTableName)
		generatedString = strings.ReplaceAll(generatedString, "[tableName]", snakeCaseTableName)
		generatedString = strings.ReplaceAll(generatedString, "[packageName]", fmt.Sprintf("%s_repository", snakeCaseTableName))
		generatedString = strings.ReplaceAll(generatedString, "[attributesMappingDTOToEntity]", columnStringList)
		generatedString = strings.ReplaceAll(generatedString, "[attributesMappingEntityToDTO]", columnStringListInverse)
		generatedString = strings.ReplaceAll(generatedString, "[createParameterSection]", createParamesterString[2:])
		generatedString = strings.ReplaceAll(generatedString, "[createParameterSectionNoType]", createParamesterStringNoType[2:])
		generatedString = strings.ReplaceAll(generatedString, "[recordFieldSection]", recordFieldString[3:])
		generatedString = strings.ReplaceAll(generatedString, "[getParameterSection]", fmt.Sprintf(", %s", getParameterString[2:]))
		generatedString = strings.ReplaceAll(generatedString, "[getParameterLogicSection]", getParameterLogicString)
		generatedString = strings.ReplaceAll(generatedString, "[primaryKey]", primaryKey)
		generatedString = strings.ReplaceAll(generatedString, "[primaryKeyType]", primaryKeyType)

		if hasDeletedAt {
			bDelete, err := ioutil.ReadFile("templates/repository_delete_template.txt")
			if err != nil {
				panic(err)
			}
			strDelete := string(bDelete)

			generatedStringDelete := strDelete
			generatedStringDelete = strings.ReplaceAll(generatedStringDelete, "[entityName]", pascalCaseTableName)
			generatedStringDelete = strings.ReplaceAll(generatedStringDelete, "[tableName]", snakeCaseTableName)

			generatedString = strings.ReplaceAll(generatedString, "[deleteSection]", generatedStringDelete)
			generatedString = strings.ReplaceAll(generatedString, "[deletedAtIsNullSection]", `db.Where("deleted_at is null")`)
		} else {
			generatedString = strings.ReplaceAll(generatedString, "[deleteSection]", "")
			generatedString = strings.ReplaceAll(generatedString, "[deletedAtIsNullSection]", ``)
		}

		if !strings.Contains(generatedString, "time.Time") {
			generatedString = strings.ReplaceAll(generatedString, "\"time\"", "")
		}

		fileName := fmt.Sprintf("%s/repository.go", tableRepositoryDirName)
		fileContent := []byte(generatedString)
		err = os.WriteFile(fileName, fileContent, 0644)
		if err != nil {
			panic(err)
		}
	}

	return nil
}
