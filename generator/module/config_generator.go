package module

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/khalid301/api-generator/generator/config"
)

func ConfigGenerator(cfg config.Config) error {
	entityDir := fmt.Sprintf(`..%s/config`, cfg.ResultDir)
	log.Printf("Generating Config file in '%s' ...", entityDir)

	b, err := ioutil.ReadFile("config/config.go")
	if err != nil {
		panic(err)
	}
	str := string(b)

	generatedString := str

	fileName := fmt.Sprintf("%s/config.go", entityDir)
	fileContent := []byte(generatedString)
	err = os.WriteFile(fileName, fileContent, 0644)
	if err != nil {
		panic(err)
	}

	return nil
}
