package module

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/iancoleman/strcase"
	"github.com/jinzhu/gorm"
	"github.com/khalid301/api-generator/generator/config"
	"github.com/khalid301/api-generator/generator/entity"
)

func EntityGenerator(cfg config.Config, db *gorm.DB) error {
	entityDir := fmt.Sprintf(`..%s/core/entity/gen_entity`, cfg.ResultDir)
	log.Printf("Generating entity files in '%s' ...", entityDir)

	tableNames := []entity.TableName{}
	sql := fmt.Sprintf("SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = '%s'", cfg.DBName)
	if err := db.Raw(sql).Scan(&tableNames).Error; err != nil {
		panic(err)
	}

	// loop every table name
	for i := 0; i < len(tableNames); i++ {
		log.Printf("Processing %s ...", tableNames[i].TableName)

		snakeCaseTableName := tableNames[i].TableName
		pascalCaseTableName := strcase.ToCamel(tableNames[i].TableName)

		b, err := ioutil.ReadFile("templates/entity_template.txt")
		if err != nil {
			panic(err)
		}
		str := string(b)

		generatedString := strings.ReplaceAll(str, "[entityName]", pascalCaseTableName)

		columnNames := []entity.ColumnNames{}
		sqlColumnNames := fmt.Sprintf("SHOW COLUMNS FROM `%s`", snakeCaseTableName)
		if err := db.Raw(sqlColumnNames).Scan(&columnNames).Error; err != nil {
			panic(err)
		}

		columnStringList := ``

		for j := 0; j < len(columnNames); j++ {
			log.Printf("- %s", columnNames[j].Field)

			snakeCaseColumnName := columnNames[j].Field
			pascalCaseColumnName := strcase.ToCamel(columnNames[j].Field)

			columnType := "string"
			for key, element := range entity.MappingDBTypeToStructType {
				if strings.Contains(columnNames[j].Type, key) {
					columnType = element
				}
			}

			columnString := fmt.Sprintf("  %s %s `json:\"%s\"`", pascalCaseColumnName, columnType, snakeCaseColumnName)
			columnStringList = fmt.Sprintf("%s\n%s", columnStringList, columnString)
		}

		generatedString = strings.ReplaceAll(generatedString, "[entityAttributes]", columnStringList)

		if strings.Contains(generatedString, "time") {
			importString := `import ("time")`
			generatedString = strings.ReplaceAll(generatedString, "[importString]", importString)
		} else {
			generatedString = strings.ReplaceAll(generatedString, "[importString]", "")
		}

		fileName := fmt.Sprintf("%s/%s.go", entityDir, snakeCaseTableName)
		fileContent := []byte(generatedString)
		err = os.WriteFile(fileName, fileContent, 0644)
		if err != nil {
			panic(err)
		}
	}

	return nil
}
