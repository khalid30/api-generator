package main

import (
	"fmt"
	"log"

	"github.com/khalid301/api-generator/generator/config"
	"github.com/khalid301/api-generator/generator/module"
	"github.com/khalid301/api-generator/generator/pkg/conn"
)

func main() {
	log.Printf("Kickstarting Generator...")

	cfg := config.Get()
	db := conn.InitMySQLDB(&cfg)
	defer conn.DbClose(db)

	if db == nil {
		panic(fmt.Errorf("error connecting to database server"))
	}

	// creating the required folder
	err := module.DirGenerator(cfg)
	if err != nil {
		panic(err)
	}

	// removing previously generated content
	err = module.ContentDirRemoval(cfg)
	if err != nil {
		panic(err)
	}

	// generating config files
	err = module.ConfigGenerator(cfg)
	if err != nil {
		panic(err)
	}

	// generating conn files
	err = module.ConnGenerator(cfg)
	if err != nil {
		panic(err)
	}

	// generating entity files
	err = module.EntityGenerator(cfg, db)
	if err != nil {
		panic(err)
	}

	// generating core repository files
	err = module.CoreRepositoryGenerator(cfg, db)
	if err != nil {
		panic(err)
	}

	// generating core usecase repository files
	err = module.UsecaseGenerator(cfg, db)
	if err != nil {
		panic(err)
	}

	// generating dto files
	err = module.DTOGenerator(cfg, db)
	if err != nil {
		panic(err)
	}

	// generating repository files
	err = module.RepositoryGenerator(cfg, db)
	if err != nil {
		panic(err)
	}

	// generating handler files
	err = module.HandlerGenerator(cfg, db)
	if err != nil {
		panic(err)
	}
	// generating router files
	err = module.RouterGenerator(cfg, db)
	if err != nil {
		panic(err)
	}
}
