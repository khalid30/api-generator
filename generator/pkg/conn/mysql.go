package conn

import (
	"fmt"
	"log"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/khalid301/api-generator/generator/config"
)

func InitMySQLDB(cfg *config.Config) *gorm.DB {

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=%+v&loc=%s", cfg.Username, cfg.Password, cfg.Host, cfg.Port, cfg.DBName, cfg.Charset, cfg.ParseTime, cfg.Loc)
	db, err := gorm.Open(cfg.DBType, dsn)
	if err != nil {
		log.Fatalf(err.Error())
		panic(err)
	} else {
		log.Printf("Successfully connected to database server")
	}

	db.DB().SetMaxIdleConns(cfg.MaxIdleConns)
	db.DB().SetMaxOpenConns(cfg.MaxOpenConns)
	db.DB().SetConnMaxLifetime(time.Duration(int(time.Minute) * cfg.ConnMaxLifetime))

	db.LogMode(cfg.LogMode)
	db.SingularTable(true)

	return db
}

func DbClose(db *gorm.DB) {
	_ = db.Close()
}
